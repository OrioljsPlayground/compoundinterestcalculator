import 'package:compound_interest_calculator/interests_bar_chart.dart';
import 'package:compound_interest_calculator/interests_calculator.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CalculatorScreen extends StatefulWidget {
  CalculatorScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CalculatorScreenState createState() => new _CalculatorScreenState();
}

final oCcy = new NumberFormat("#,##0.00", "es_ES");


class _CalculatorScreenState extends State<CalculatorScreen> {
  bool _showValidationInitialInvestmentError = false;
  bool _showValidationMonthlyContributionError = false;
  bool _showValidationInterestRateError = false;
  bool _showValidationYearsError = false;

  var _currency = '€';

  var _fv = '';
  var _interestsEarned = '';
  var _moneyInvested = '';
  var _yearOverYearFV = [];

  int _inputInitialInvestmentValue = 0;
  int _inputMonthlyContributionValue = 0;
  double _inputInterestRateValue = 0.00;
  int _inputYearsValue = 0;


  TextEditingController _cII ;
  TextEditingController _cMC ;
  TextEditingController _cIR ;
  TextEditingController _cY ;

  @override
  void initState() {
    _cII = new TextEditingController();
    _cMC = new TextEditingController();
    _cIR = new TextEditingController();
    _cY = new TextEditingController();

    super.initState();
    setDefaultValues();
  }

  void _updateFv() {
    setState(() {
      var values = [
        _inputInitialInvestmentValue,
        _inputMonthlyContributionValue,
        _inputInterestRateValue,
        _inputYearsValue
      ];
      if (values.contains(null) || values.contains("")) {
        _fv = '';
        _interestsEarned = '';
        _moneyInvested = '';
        _yearOverYearFV = [];
        return;
      }

      var _result = calculateCompoundInterest(
        years: _inputYearsValue,
        monthlyContribution: _inputMonthlyContributionValue.toDouble(),
        initialInvestment: _inputInitialInvestmentValue.toDouble(),
        annualInterestRate: _inputInterestRateValue,
      );

      _fv = "${oCcy.format(_result[0])} $_currency";
      _interestsEarned = "${oCcy.format(_result[2])} $_currency";
      _moneyInvested = "${oCcy.format(_result[1])} $_currency";
      _yearOverYearFV = _result[3];
    });
  }

  void _updateInputValue(
      {String initialInvestment,
      String monthlyContribution,
      String interestRate,
      String years}) {
    setState(() {
      // TODO: compact this method, using list comprehension or similar

      // initialInvestment
      if (initialInvestment != null) {
        try {
          _inputInitialInvestmentValue = int.parse(initialInvestment);
          _showValidationInitialInvestmentError = false;
        } on Exception catch (e) {
          print('Error: $e');
          _showValidationInitialInvestmentError = true;
        }
      }

      // monthlyContribution
      if (monthlyContribution != null) {
        try {
          _inputMonthlyContributionValue = int.parse(monthlyContribution);
          _showValidationMonthlyContributionError = false;
        } on Exception catch (e) {
          print('Error: $e');
          _showValidationMonthlyContributionError = true;
        }
      }

      // interestRate
      if (interestRate != null) {
        try {
          _inputInterestRateValue = double.parse(interestRate);
          _inputInterestRateValue = _inputInterestRateValue / 100;
          _showValidationInterestRateError = false;
        } on Exception catch (e) {
          print('Error: $e');
          _showValidationInterestRateError = true;
        }
      }

      // years
      if (years != null) {
        try {
          _inputYearsValue = int.parse(years);
          if (_inputYearsValue < 0) {
            _inputYearsValue = null;
            throw Exception("Invalid year");
          }
          _showValidationYearsError = false;
        } on Exception catch (e) {
          print('Error: $e');
          _showValidationYearsError = true;
        }
      }
    });

    _updateFv();

    _saveLatestValidInput();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Compound Interest Calculator"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(32.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Table(
                children: [
                  TableRow(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: TextField(
                          key: Key("InitialInvestmentWidget"),
                          controller: _cII,
                          onChanged: (text) {
                            _updateInputValue(initialInvestment: text);
                          },
                          decoration: InputDecoration(
                              errorText: _showValidationInitialInvestmentError
                                  ? 'Invalid initial investment'
                                  : null,
                              suffix: Text("€"),
                              labelText: 'Initial investment',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(0.0))),
                          maxLines: 1,
                          keyboardType: TextInputType.numberWithOptions(
                              decimal: true, signed: false),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: TextField(
                          key: Key("MonthlyContributionWidget"),
                          controller: _cMC,
                          onChanged: (text) {
                            _updateInputValue(monthlyContribution: text);
                          },
//                          onChanged: (text) {
//                            _updateInputValue(monthlyContribution: text);
//                          },
                          decoration: InputDecoration(
                              errorText: _showValidationMonthlyContributionError
                                  ? 'Invalid monthly contribution'
                                  : null,
                              suffix: Text("€/month"),
                              labelText: 'Monthly contribution',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(0.0))),
                          maxLines: 1,
                          keyboardType: TextInputType.numberWithOptions(
                              decimal: true, signed: false),
                        ),
                      ),
                    ],
                  ),
                  TableRow(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: TextField(
                          key: Key("InterestRateWidget"),
                          controller: _cIR,
                          onChanged: (text) {
                            _updateInputValue(interestRate: text);
                          },
                          maxLines: 1,
                          maxLength: 2,
                          decoration: InputDecoration(
                              errorText: _showValidationInterestRateError
                                  ? 'Invalid interest rate'
                                  : null,
                              labelText: 'Interest Rate',
                              suffix: Text("%"),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(0.0))),
                          keyboardType: TextInputType.numberWithOptions(
                              decimal: true, signed: false),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: TextField(
                          key: Key("YearsWidget"),
                          controller: _cY,
                          onChanged: (text) {
                            _updateInputValue(years: text);
                          },
                          maxLines: 1,
                          maxLength: 2,
                          decoration: InputDecoration(
                              errorText: _showValidationYearsError
                                  ? 'Invalid years'
                                  : null,
                              labelText: 'Years',
                              suffix: Text("years"),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(0.0))),
                          keyboardType: TextInputType.numberWithOptions(
                              decimal: true, signed: false),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: Table(
                  children: [
                    TableRow(children: [
                      Text(
                        "Total future value: ",
                        style: Theme.of(context).textTheme.body1,
                      ),
                      Text(
                        _fv,
                        style: Theme.of(context).textTheme.body2,
                      ),
                    ]),
                    TableRow(children: [
                      Text(
                        "Total money invested: ",
                        style: Theme.of(context).textTheme.body1,
                      ),
                      Text(
                        _moneyInvested,
                        style: Theme.of(context).textTheme.body2,
                      ),
                    ]),
                    TableRow(children: [
                      Text(
                        "Total interests earned: ",
                        style: Theme.of(context).textTheme.body1,
                      ),
                      Text(
                        _interestsEarned,
                        style: Theme.of(context).textTheme.body2,
                      ),
                    ]),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Container(
                  constraints: BoxConstraints.expand(
                      width: double.infinity, height: 300.0),
                  child: InterestsBarChart.buildChart(
                    yearOverYearFV: _yearOverYearFV,
                    animate: true,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void setDefaultValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _inputInitialInvestmentValue = prefs.getInt('last_investment_value') ?? 500;
      _cII.text = _inputInitialInvestmentValue.toString();
      _inputMonthlyContributionValue = prefs.getInt('last_contribution_value') ?? 150;
      _cMC.text = _inputMonthlyContributionValue.toString();
      var interest = prefs.getInt('last_interest_rate_value') ?? 6;
      _inputInterestRateValue = interest/100;
      _cIR.text = interest.toString();
      _inputYearsValue = prefs.getInt('last_years_value') ?? 10;
      _cY.text = _inputYearsValue.toString();
    });
    _updateFv();
  }

  void _saveLatestValidInput() async {
    if (_showValidationYearsError || _showValidationInterestRateError || _showValidationInitialInvestmentError || _showValidationMonthlyContributionError) {
      return;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('last_investment_value', _inputInitialInvestmentValue);
    await prefs.setInt('last_contribution_value', _inputMonthlyContributionValue);
    await prefs.setInt('last_interest_rate_value', (_inputInterestRateValue*100).toInt());
    await prefs.setInt('last_years_value', _inputYearsValue);
  }
}
