import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class InterestsBarChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  InterestsBarChart(this.seriesList, {this.animate});

  factory InterestsBarChart.buildChart({@required yearOverYearFV, @required bool animate}) {
    return new InterestsBarChart(
      _createDataSeries(yearOverYearFV),
      animate: animate,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      // Configure a stroke width to enable borders on the bars.
      defaultRenderer: new charts.BarRendererConfig(
          groupingType: charts.BarGroupingType.stacked, strokeWidthPx: 2.0),
    );
  }

  /// Create series list with multiple series
  static List<charts.Series<OrdinalMoney, String>> _createDataSeries(yearOverYearFV) {
    final List<OrdinalMoney> interestsData = [];
    final List<OrdinalMoney> contributedData = [];

    int yearC = 1;
    for (var yearData in yearOverYearFV) {
      contributedData.add(new OrdinalMoney(yearC.toString(), yearData[1].toInt()));
      interestsData.add(new OrdinalMoney(yearC.toString(), yearData[2].toInt()));
      yearC += 1;
    }

    return [
      new charts.Series<OrdinalMoney, String>(
        id: 'Interests',
        domainFn: (OrdinalMoney sales, _) => sales.year,
        measureFn: (OrdinalMoney sales, _) => sales.money,
        data: interestsData,
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
//        fillColorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault.lighter,
      ),

      new charts.Series<OrdinalMoney, String>(
        id: 'Contributed',
        measureFn: (OrdinalMoney sales, _) => sales.money,
        data: contributedData,
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalMoney sales, _) => sales.year,
      ),
    ];
  }
}

class OrdinalMoney {
  final String year;
  final int money;

  OrdinalMoney(this.year, this.money);
}
