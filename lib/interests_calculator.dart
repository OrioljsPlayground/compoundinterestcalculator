import 'dart:math';

import 'package:meta/meta.dart';

List _calculateFVInterest(
    {@required double initialInvestment,
    @required double annualInterestRate,
    @required int years}) {
  /// returns [double balance, double investment, double interest]
  ///
  /// FV = future value
  ///
  /// formula = FV   =   P (1  +  r / n)^Yn
  ///  where P is the starting principal, r is the annual interest rate,
  ///  Y is the number of years invested, and n is the number of compounding periods per year.
  ///  FV is the future value, meaning the amount the principal grows to after Y years.

  assert(initialInvestment >= 0);
  assert(annualInterestRate <= 1);
  assert(annualInterestRate >= 0);
  assert(years >= 0);
  assert(years is int);

  const _compounding_periods = 1;
  var fv = initialInvestment *
      pow((1 + (annualInterestRate / _compounding_periods)),
          (years * _compounding_periods));

  var interests = fv - initialInvestment;
  return [fv, initialInvestment, interests];
}

List calculateCompoundInterest(
    {@required double initialInvestment,
    @required double monthlyContribution,
    @required double annualInterestRate,
    @required int years}) {
  assert(monthlyContribution >= 0);
  assert(initialInvestment >= 0);
  assert(annualInterestRate <= 1);
  assert(annualInterestRate >= 0);
  assert(years >= 0);
  assert(years is int);

  var totalInvested = initialInvestment;
  var totalInterests = 0.0;
  var lastFv = initialInvestment;

  var lastFvRounded;
  var totalInterestsRounded;

  var yearOverYearFV = [];

  for (var year in List<int>.generate(years, (i) => i + 1)) {
    var fv = _calculateFVInterest(
        initialInvestment: lastFv,
        annualInterestRate: annualInterestRate,
        years: 1);
    lastFv = fv[0] + (monthlyContribution * 12);
    totalInvested += monthlyContribution * 12;
    totalInterests += fv[2];

  lastFvRounded = num.parse(lastFv.toStringAsFixed(2));
  totalInterestsRounded = num.parse(totalInterests.toStringAsFixed(2));

  yearOverYearFV.add([lastFvRounded, totalInvested, totalInterestsRounded]);
  }

  return [lastFvRounded, totalInvested, totalInterestsRounded, yearOverYearFV];
}
