import 'package:compound_interest_calculator/calculator_screen.dart';
import 'package:flutter/material.dart';
// import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:firebase_analytics/observer.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Compound Interest Calculator',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: CalculatorScreen(),
      // navigatorObservers: [
        // FirebaseAnalyticsObserver(analytics: analytics),
      // ],
    );
  }
}
