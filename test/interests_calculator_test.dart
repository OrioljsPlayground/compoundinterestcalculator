// This is a basic Flutter widget test.
// To perform an interaction with a widget in your test, use the WidgetTester utility that Flutter
// provides. For example, you can send tap and scroll gestures. You can also use WidgetTester to
// find child widgets in the widget tree, read text, and verify that the values of widget properties
// are correct.



import 'package:compound_interest_calculator/interests_calculator.dart';
import 'package:flutter_test/flutter_test.dart';


void main() {
  // TODO: test result types are correct
  // TODO: test with something like hypothesis
  // TODO: test incorrect inputs
  // TODO: test edge cases
  // TODO: test more cases
  test("correct types", () {
    var result = calculateCompoundInterest(initialInvestment: 10000.0, annualInterestRate: 0.07, monthlyContribution: 0.0, years: 10);
    expect(result[0] is double, equals(true));
    expect(result[1] is double, equals(true));
    expect(result[2] is double, equals(true));
  });

  test("With no monthly contributions", () {
    var result = calculateCompoundInterest(initialInvestment: 10000.0, annualInterestRate: 0.07, monthlyContribution: 0.0, years: 10);
    expect(result, equals([19671.51, 10000.00, 9671.51]));
    result = calculateCompoundInterest(initialInvestment: 0.0, annualInterestRate: 0.07, monthlyContribution: 0.0, years: 10);
    expect(result, equals([0, 0, 0]));
    result = calculateCompoundInterest(initialInvestment: 10000.0, annualInterestRate: 0.00, monthlyContribution: 0.0, years: 10);
    expect(result, equals([10000, 10000, 0]));
    result = calculateCompoundInterest(initialInvestment: 10000.0, annualInterestRate: 0.077, monthlyContribution: 0.0, years: 10);
    expect(result, equals([20996.99, 10000.00, 10996.99]));
  });

  test("with monthly contributions", () {
    // calculators:
    // http://moneychimp.com/calculator/compound_interest_calculator.htm
    // https://www.investor.gov/additional-resources/free-financial-planning-tools/compound-interest-calculator
    // (this one returns slightly different results) https://www.calculatestuff.com/financial/compound-interest-calculator


    var result = calculateCompoundInterest(initialInvestment: 10000.0, annualInterestRate: 0.07, monthlyContribution: 800.0, years: 10);
    expect(result, equals([152309.41, 106000.00, 46309.41]));
    expect(result[0]-result[1], equals(result[2]));
  });
}
