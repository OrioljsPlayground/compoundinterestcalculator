// This is a basic Flutter widget test.
// To perform an interaction with a widget in your test, use the WidgetTester utility that Flutter
// provides. For example, you can send tap and scroll gestures. You can also use WidgetTester to
// find child widgets in the widget tree, read text, and verify that the values of widget properties
// are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:compound_interest_calculator/main.dart';

void main() {
  testWidgets('Correct input', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(new MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('€'), findsNothing);

    await tester.enterText(find.byKey(Key("InitialInvestmentWidget")), "10000");
    await tester.enterText(find.byKey(Key("MonthlyContributionWidget")), "800");
    await tester.enterText(find.byKey(Key("InterestRateWidget")), "7");
    await tester.enterText(find.byKey(Key("YearsWidget")), "10");
    await tester.pump(); // Trigger a frame

    expect(find.text('152309.41 €'), findsOneWidget);
    expect(find.text('46309.41 €'), findsOneWidget);
  });

  testWidgets('Invalid inputs', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(new MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('€'), findsNothing);

    await tester.enterText(find.byKey(Key("InitialInvestmentWidget")), "5-5");
//    await tester.enterText(find.byKey(Key("MonthlyContributionWidget")), "800");
//    await tester.enterText(find.byKey(Key("InterestRateWidget")), "7");
    await tester.pump(); // Trigger a frame

    expect(find.text('Invalid initial investment'), findsOneWidget);

    await tester.enterText(find.byKey(Key("YearsWidget")), "-1");
    await tester.pump(); // Trigger a frame

    expect(find.text('Invalid years'), findsOneWidget);
  });
}
